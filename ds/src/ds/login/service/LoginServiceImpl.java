package ds.login.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import ds.login.dao.LoginDAO;

@Service("loginService")
public class LoginServiceImpl implements LoginService {
	
	Logger log = Logger.getLogger(this.getClass());

	@Resource(name="loginDAO")
    private LoginDAO loginDAO;

	/**
	 * Login 정보 확인 후 사용자 Login 정보 Return 
	 */
	@Override
	public Map<String, Object> selectUserInfo(Map<String, Object> map) throws Exception {
		Map<String, Object> resultMap = loginDAO.selectUserInfo(map);
		return resultMap;
	}

	/**
	 * 회원가입: Email 중복 Check
	 */
	@Override
	public int checkEmail(Map<String, Object> map) throws Exception {
		
		int iChkRtn = loginDAO.checkEmail(map);
		
		return iChkRtn;
	}

	/**
	 * 회원가입: 사용자 등록
	 */
	@Override
	public void insertUser(Map<String, Object> map) throws Exception {
		
		loginDAO.insertUser(map) ;
	}

	/**
	 * E-mail 찾기: 가입자 Email 정보 Select 
	 */
	@Override
	public List<Map<String, Object>> selectFindEmail(Map<String, Object> map) throws Exception {
		return loginDAO.selectFindEmail(map);
	}

	/**
	 * 비밀번호 찾기
	 */
	@Override
	public Map<String, Object> findPassword(Map<String, Object> map) throws Exception {
		Map<String, Object> resultMap = loginDAO.findPassword(map);
		return resultMap;
	}

	/**
	 * 비밀번호 찾기: 난수 생성 Password, 암호초기화여부 Update
	 */
	@Override
	public void updateFindPassword(Map<String, Object> map) throws Exception {
		loginDAO.updateFindPassword(map);
		
	}

	/**
	 * 비밀번호 변경
	 */
	@Override
	public void changePassword(Map<String, Object> map) throws Exception {
		loginDAO.changePassword(map);
		
	}

}
