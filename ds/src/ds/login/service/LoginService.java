package ds.login.service;

import java.util.List;
import java.util.Map;

public interface LoginService {

	Map<String, Object> selectUserInfo(Map<String, Object> map) throws Exception;

	int checkEmail(Map<String, Object> map) throws Exception;

	void insertUser(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectFindEmail(Map<String, Object> map) throws Exception;

	Map<String, Object> findPassword(Map<String, Object> map) throws Exception;

	void updateFindPassword(Map<String, Object> map) throws Exception;

	void changePassword(Map<String, Object> map) throws Exception;

}
