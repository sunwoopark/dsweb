package ds.login.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import ds.common.dao.AbstractDAO;

@Repository("loginDAO")
public class LoginDAO  extends AbstractDAO{

	@SuppressWarnings("unchecked")
	public Map<String, Object> selectUserInfo(Map<String, Object> map) {
		return (Map<String, Object>) selectOne("login.selectUserInfo", map);
	}

	public int checkEmail(Map<String, Object> map) {
		return (int)selectOne("login.checkEmail", map);
	}

	public void insertUser(Map<String, Object> map) {
		insert("login.insertUser", map);
		
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectFindEmail(Map<String, Object> map) {
		return (List<Map<String, Object>>) selectList("login.selectFindEmail", map);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> findPassword(Map<String, Object> map) {
		return (Map<String, Object>) selectOne("login.findPassword", map);
	}

	public void updateFindPassword(Map<String, Object> map) {
		update("login.updateFindPassword", map);
	}

	public void changePassword(Map<String, Object> map) {
		update("login.changePassword", map);
		
	}

}
