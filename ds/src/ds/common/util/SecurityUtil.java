package ds.common.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

//import sun.misc.BASE64Decoder;
//import sun.misc.BASE64Encoder;

@SuppressWarnings("restriction")
public class SecurityUtil {
	
	/** 비밀번호 암호화 12월 17일 추가 **/
	public String encryptSHA256(String str){
		String sha = "";
		
		try{
			MessageDigest sh = MessageDigest.getInstance("SHA-256");
			sh.update(str.getBytes());
			byte byteDate[] = sh.digest();
			StringBuffer sb = new StringBuffer();
			
			for (int i = 0; i < byteDate.length; i++){
				sb.append(Integer.toString((byteDate[i]&0xff) + 0x100, 16).substring(1));
			}
			sha = sb.toString();
			
		}catch(NoSuchAlgorithmException e){
			System.out.println("Encry Error - NoSuchAlgorithmException");
			sha = null;
		}
		
		return sha;
	}
	
	public static final String ENCRYPT_METHOD = "MD5";

    private final static int ITERATION_NUMBER = 1000;

    public static char[] arr = {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
            , 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm'
            , 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
            , 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'
            , 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
    };

    private static String BASIC_SALT_STR = "pR0jectDataC2nter";

    public static void main(String[] args) {
        try {
            System.out.println(createPassword("1234"));

            System.out.println(createBroadcastId("000000009"));
            System.out.println(getChatPort("BYWUSQOMmv"));
            System.out.println("111:::::" + getEncryptString("1q2w3e4r5t!"));
            System.out.println(createPasswordWithSalt("qwer1234")[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getChatPort(String str) {
        char[] chars = str.toCharArray();
        StringBuffer hex = new StringBuffer();
        int a = 0;
        for (int i = 0; i < chars.length; i++) {
            if (i > 7) {
                a = (int) chars[i] - 61;
                hex.append((char) a);
            }
        }
        return hex.toString();
    }

    public static String createBroadcastId(String str) {

        char[] chars = str.toCharArray();

        StringBuffer hex = new StringBuffer();

        int a = 0;
        for (int i = 0; i < chars.length; i++) {
            a = ((int) chars[i] * (Integer.valueOf(str) * (i + 1))) % 62;
            if (i > 6) a = (int) chars[i] - 26;
            System.out.println((int) chars[i] + "," + a + "," + (int) arr[a]);
            hex.append(arr[a]);
        }
        return hex.toString();
    }

    
    /**
     * @param pw
     * @return
     * @deprecated 단순 암호화
     */
    public static String createPassword(String pw) {
        StringBuffer resultPw = new StringBuffer();

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.reset();
            byte[] encodePw = md.digest(pw.getBytes("UTF-8"));

            for (int i = 0; i < encodePw.length; i++) {
                resultPw.append(Integer.toHexString(encodePw[i] & 0xFF));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("PW Encode Error! in UserMemeberBiz.java at userMemberModify()");
        }

        return resultPw.toString();
    }
    
    /**
     * String[0] password
     * String[1] salt
     *
     * @param inputPassword
     * @return
     */
    public static String[] createPasswordWithSalt(String inputPassword) {
        String[] ret = null;

        try {
            byte[] bSalt = BASIC_SALT_STR.getBytes("UTF-8");
            byte[] bDigest = getHash(ITERATION_NUMBER, inputPassword, bSalt);
            String sDigest = byteToBase64(bDigest);
            String sSalt = byteToBase64(bSalt);

            if (sDigest != null && !sDigest.isEmpty() && sSalt != null && !sSalt.isEmpty()) {
                ret = new String[2];
                ret[0] = sDigest;
                ret[1] = sSalt;
            }
        } catch (Exception e) {
        }

        return ret;
    }

    public static boolean checkPasswordWithSalt(String inputPassword, String password, String salt) {
        boolean ret = false;

        try {
            byte[] bDigest = base64ToByte(password);
            byte[] bSalt = base64ToByte(salt);

            // Compute the new DIGEST
            byte[] proposedDigest = getHash(ITERATION_NUMBER, inputPassword, bSalt);
            
            ret = Arrays.equals(proposedDigest, bDigest);
        } catch (Exception e) {
        }

        return ret;
    }

    public static byte[] getHash(int iterationNb, String password, byte[] salt) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.reset();
        digest.update(salt);
        byte[] input = digest.digest(password.getBytes("UTF-8"));
        for (int i = 0; i < iterationNb; i++) {
            digest.reset();
            input = digest.digest(input);
        }
        return input;
    }

	public static byte[] base64ToByte(String data) throws IOException {
        BASE64Decoder decoder = new BASE64Decoder();
        return decoder.decodeBuffer(data);
    }

	public static String byteToBase64(byte[] data) {
        BASE64Encoder endecoder = new BASE64Encoder();
        return endecoder.encode(data);
    }

    public static String getEncryptString(String str) throws Exception {
        if (str == null) {
            throw new Exception("no input str");
        }
        return createPasswordWithSalt(str)[0];
    }

    //checkValue 로 주어진 값이  encryptString 와 같은 값인지 확인
    public static boolean equals(String checkValue, String encryptString) throws Exception {
        if (checkValue != null) {
            String value = getEncryptString(checkValue);
            if (value != null) {
                if (value.equals(encryptString)) {
                    return true;
                }
            }
        }
        return false;
    }

    //실제 md5 로 변경
    @SuppressWarnings("unused")
    private static byte[] digest(byte[] input) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(ENCRYPT_METHOD);
        return md.digest(input);
    }

    //base64로  encoding
    @SuppressWarnings({"unused"})
    private static String base64Encode(byte[] datas) throws Exception {
        String b64enc = null;
        if (datas != null) {
            BASE64Encoder encoder = new BASE64Encoder();
            b64enc = encoder.encode(datas);
        }
        return b64enc;
    }

    /**
     * 비밀번호 체크
     *
     * @param pwd
     * @param id
     * @return
     */
    public static Map<String, String> checkPassword(String pwd, String id) {
        return checkPassword(pwd, pwd, id);
    }

    /**
     * 비밀번호 체크
     *
     * @param pwd
     * @param confirmPwd
     * @param id
     * @return
     */
    public static Map<String, String> checkPassword(String pwd, String confirmPwd, String id) {
        final String SUCCESS_CODE = "0";
        final String FAIL_CODE = "1";
        Map<String, String> resultMap = new HashMap<String, String>();
        if (pwd == null || pwd.length() == 0 || id == null || id.length() == 0) {
            resultMap.put("code", FAIL_CODE);
            resultMap.put("msg", "비밀번호와 아이디를 바르게 입력해주세요.");
            resultMap.put("eng-msg", "Please enter the correct password and username.");
            return resultMap;
        }
        if (!pwd.equals(confirmPwd)) {
            resultMap.put("code", FAIL_CODE);
            resultMap.put("msg", "입력하신 비밀번호와 비밀번호확인이 일치하지 않습니다.");
            resultMap.put("eng-msg", "Entered Password and Confirm Password does not match.");
            return resultMap;
        }

        if (pwd.indexOf(id) > -1 || id.indexOf(pwd) > -1) {
            resultMap.put("code", FAIL_CODE);
            resultMap.put("msg", "비밀번호는 아이디와 다르게 설정해주세요.");
            resultMap.put("eng-msg", "Please set a different user name and password.");
            return resultMap;
        }

        if (!pwd.matches("^[a-z0-9A-Z\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\-\\_\\=\\+\\|\\[\\]\\{\\}\\'\\\"\\;\\:\\/\\?\\.\\,\\>\\<\\,]{10,20}$")) {
            resultMap.put("code", FAIL_CODE);
            resultMap.put("msg", "영문자와 숫자,특수문자를 조합하여 10자리 이상 20자리 이하로 입력해주세요.\n사용가능한 특수문자는 !@#$%^&*()-_=+|[]{}'\";:/?.,><,입니다.");
            resultMap.put("eng-msg", "Letters, numbers, special characters, any combination of more than 10 digits and less than 20 digits.\n available special characters is !@#$%^&*()-_=+|[]{}'\";:/?.,><,");
            return resultMap;
        }

        String tmpPwd = pwd.replaceAll("\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\-\\_\\=\\+\\|\\[\\]\\{\\}\\'\\\"\\;\\:\\/\\?\\.\\,\\>\\<\\,", "");
        if (tmpPwd.replaceAll("[A-Za-z]", "").equals("") || tmpPwd.replaceAll("[0-9]", "").equals("")) {
            resultMap.put("code", FAIL_CODE);
            resultMap.put("msg", "영문자와 숫자는 반드시 조합하여 입력해주세요.");
            resultMap.put("eng-msg", "Please enter a combination of letters and numbers.");
            return resultMap;
        }

        int continueCharCnt = 0;
        int sameCharCnt = 0;
        byte preChar = -1;
        for (byte pwdChar : pwd.getBytes()) {
            if (pwdChar == preChar) {
                if (++sameCharCnt > 0) {
                    resultMap.put("code", FAIL_CODE);
                    resultMap.put("msg", "동일문자나 숫자(aa또는 33)를 2번 이상 사용할 수 없습니다.");
                    resultMap.put("eng-msg", "The same letter or number (aa or 33) can not be used more than two times.");
                    return resultMap;
                }
            } else {
                sameCharCnt = 0;
            }
            if (pwdChar == preChar + 1) {
                if (++continueCharCnt > 1) {
                    resultMap.put("code", FAIL_CODE);
                    resultMap.put("msg", "연속된 문자열(123 또는 321, abc, cba 등)을\n 3자 이상 사용할 수 없습니다.");
                    resultMap.put("eng-msg", "Consecutive string (123 or 321, abc, cba, etc.) can not be used three or more characters.");
                    return resultMap;
                }
            } else {
                continueCharCnt = 0;
            }
            preChar = pwdChar;
        }
        resultMap.put("code", SUCCESS_CODE);
        return resultMap;
    }
    
    
	
}