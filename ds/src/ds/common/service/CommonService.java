package ds.common.service;

import java.util.List;
import java.util.Map;

public interface CommonService {
	 
	/**
	 * 공통코드 목록 Select
	 * @param string
	 * @return
	 * @throws Exception
	 */
	List<Map<String, Object>> selectCmnCdSelectBox(String string) throws Exception;

	/**
	 * User Log Insert
	 * @param map
	 * @return
	 * @throws Exception
	 */
	Object insertUseLog(Map<String, Object> map) throws Exception;

	/**
	 * Menu 권한 Select
	 * @param map
	 * @return
	 * @throws Exception
	 */
	Map<String, Object> selectMenuArth(Map<String, Object> map) throws Exception;

	/**
	 * 공통코드 단건 Select: 관리자 이메일 정보 Select
	 * @param adminParmMap
	 * @return
	 */
	Map<String, Object> selectCmnCd(Map<String, Object> adminParmMap) throws Exception;
	
}