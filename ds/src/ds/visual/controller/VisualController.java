package ds.visual.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import ds.common.common.CommandMap;
import ds.common.service.CommonService;
import ds.visual.service.VisualService;

@Controller
public class VisualController {

	Logger log = Logger.getLogger(this.getClass());

	@Resource(name = "visualService")
	private VisualService visualService;

	@Resource(name = "commonService")
	private CommonService commonService;
	/* �씤援� �쁽�솴 媛쒖슂 - �씤援� �쁽�솴 */

	@RequestMapping(value = "/main.do")
	public ModelAndView selectMain(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("main/main");

		return mv;

	}

	@RequestMapping(value = "/visual/accident.do")
	public ModelAndView selectAccident(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("trafficaccident/accident");

		List<Map<String, Object>> list = visualService.selectAccident(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> list2 = visualService.selectAccident2(commandMap.getMap());
		mv.addObject("list2", list2);
		List<Map<String, Object>> year = visualService.selectAccidentYear(commandMap.getMap());
		mv.addObject("year", year);

		/* 3-1. 월별 교통사고 발생 건수 */
		List<Map<String, Object>> list3_1 = visualService.selectAccidentTrafficMM("2018");
		List<Map<String, Object>> list3_2 = visualService.selectAccidentTrafficMM("2019");
		List<Map<String, Object>> list3_3 = visualService.selectAccidentTrafficMM("총합계");
		mv.addObject("list3_1", list3_1);
		mv.addObject("list3_2", list3_2);
		mv.addObject("list3_3", list3_3);

		/* 3-2. 요일별 교통사고 발생 건수 */
		List<Map<String, Object>> list4_1 = visualService.selectAccidentTrafficDay("2018");
		List<Map<String, Object>> list4_2 = visualService.selectAccidentTrafficDay("2019");
		List<Map<String, Object>> list4_3 = visualService.selectAccidentTrafficDay("총합계");
		mv.addObject("list4_1", list4_1);
		mv.addObject("list4_2", list4_2);
		mv.addObject("list4_3", list4_3);

		return mv;

	}

	@RequestMapping(value = "/visual/proc-accident.do")
	public ModelAndView selectAccidentAjax(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("jsonView");

		List<Map<String, Object>> list = visualService.selectAccident(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> list2 = visualService.selectAccident2(commandMap.getMap());
		mv.addObject("list2", list2);

		return mv;
	}

	@RequestMapping(value = "/visual/disabled.do")
	public ModelAndView selectDisabled(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("disabled/disabled");

		List<Map<String, Object>> list = visualService.selectDisabled(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> list1 = visualService.selectFine(commandMap.getMap());
		mv.addObject("list1", list1);
		List<Map<String, Object>> list2 = visualService.selectParking(commandMap.getMap());
		mv.addObject("list2", list2);

		/* 7-1 행정동별불법주정차CCTV현황 */
		List<Map<String, Object>> list3 = visualService.selectCctvCntDo(commandMap.getMap());
		mv.addObject("list3", list3);

		/* 7-2 월별장애인주차구역불법주차단속현황 */
		List<Map<String, Object>> list4_1 = visualService.selectDisabledParkingMM("2018");
		List<Map<String, Object>> list4_2 = visualService.selectDisabledParkingMM("2019");
		List<Map<String, Object>> list4_3 = visualService.selectDisabledParkingMM("2020");
		mv.addObject("list4_1", list4_1);
		mv.addObject("list4_2", list4_2);
		mv.addObject("list4_3", list4_3);
		
		/* 7-2 행정동별불법주정차CCTV현황 */
		List<Map<String, Object>> list44 = visualService.selectDisabledParkingMM2(commandMap.getMap());
		mv.addObject("list44", list44);

		/* 7-3 장애인주차구역불법주차 행정동별 단속현황 */
		List<Map<String, Object>> list5 = visualService.selectDisabledParkingState(commandMap.getMap());
		mv.addObject("list5", list5);

		/* 7-4 행정동별장애인주차장현황 */
		List<Map<String, Object>> list6 = visualService.selectDisabledParkingDo(commandMap.getMap());
		mv.addObject("list6", list6);

		return mv;

	}

	@RequestMapping(value = "/visual/proc-disabled.do")
	public ModelAndView selectDisabledAjax(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("jsonView");

		List<Map<String, Object>> list = visualService.selectDisabled(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> list1 = visualService.selectFine(commandMap.getMap());
		mv.addObject("list1", list1);
		List<Map<String, Object>> list2 = visualService.selectParking(commandMap.getMap());
		mv.addObject("list2", list2);

		return mv;
	}

	@RequestMapping(value = "/visual/traffic.do")
	public ModelAndView selectFraffic(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("complaint/status/fraffic");

		List<Map<String, Object>> list = visualService.selectTraffic(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> list2 = visualService.selectTraffic2(commandMap.getMap());
		mv.addObject("list2", list2);
		List<Map<String, Object>> year = visualService.selectTrafficYear(commandMap.getMap());
		mv.addObject("year", year);
		
		/* 1. 교통민원 */
		List<Map<String, Object>> list3_1 = visualService.selectComplaintTraffic("2018");
		List<Map<String, Object>> list3_2 = visualService.selectComplaintTraffic("2019");
		List<Map<String, Object>> list3_3 = visualService.selectComplaintTraffic("2020");
		mv.addObject("list3_1", list3_1);
		mv.addObject("list3_2", list3_2);
		mv.addObject("list3_3", list3_3);
		
		
		return mv;

	}

	@RequestMapping(value = "/visual/proc-traffic.do")
	public ModelAndView selectTrafficAjax(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("jsonView");

		List<Map<String, Object>> list = visualService.selectTraffic(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> list2 = visualService.selectTraffic2(commandMap.getMap());
		mv.addObject("list2", list2);

		return mv;
	}

	@RequestMapping(value = "/visual/parking.do")
	public ModelAndView selectParking(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("complaint/status/parking");

		List<Map<String, Object>> list = visualService.selectComParking(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> list2 = visualService.selectComParking2(commandMap.getMap());
		mv.addObject("list2", list2);
		List<Map<String, Object>> year = visualService.selectComParkingYear(commandMap.getMap());
		mv.addObject("year", year);

		/* 2. 주차민원 */
		List<Map<String, Object>> list3_1 = visualService.selectComplaintParking("2018");
		List<Map<String, Object>> list3_2 = visualService.selectComplaintParking("2019");
		List<Map<String, Object>> list3_3 = visualService.selectComplaintParking("2020");
		mv.addObject("list3_1", list3_1);
		mv.addObject("list3_2", list3_2);
		mv.addObject("list3_3", list3_3);

		return mv;

	}

	@RequestMapping(value = "/visual/proc-parking.do")
	public ModelAndView selectParkingAjax(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("jsonView");

		List<Map<String, Object>> list = visualService.selectComParking(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> list2 = visualService.selectComParking2(commandMap.getMap());
		mv.addObject("list2", list2);

		return mv;
	}

	@RequestMapping(value = "/visual/fine.do")
	public ModelAndView selectFine(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("fine/fine");

		List<Map<String, Object>> list = visualService.selectHangfine(commandMap.getMap());
		mv.addObject("list", list);

		/* 5-1. 행정동별 평균 부과금액 및 대상건물수 현황 */
		List<Map<String, Object>> list3 = visualService.selectIllegalParkingAvg(commandMap.getMap());
		mv.addObject("list3", list3);

		return mv;

	}

	@RequestMapping(value = "/visual/working.do")
	public ModelAndView selectWorking(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("workcctv/working");

		List<Map<String, Object>> list = visualService.selectWorkchild(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> list1 = visualService.selectWorkop(commandMap.getMap());
		mv.addObject("list1", list1);

		// 8-1 행정동별 어린이 노인 시설 현황
		List<Map<String, Object>> list3_1 = visualService.selectWalkBuilDo("경로당");
		List<Map<String, Object>> list3_2 = visualService.selectWalkBuilDo("어린이집");
		List<Map<String, Object>> list3_3 = visualService.selectWalkBuilDo("초등학교");
		List<Map<String, Object>> list3_4 = visualService.selectWalkBuilDo("총합계");
		mv.addObject("list3_1", list3_1);
		mv.addObject("list3_2", list3_2);
		mv.addObject("list3_3", list3_3);
		mv.addObject("list3_4", list3_4);

		// 8-2육교 설치현황
		List<Map<String, Object>> list4 = visualService.selectOverpassState(commandMap.getMap());
		mv.addObject("list4", list4);

		return mv;

	}

	@RequestMapping(value = "/visual/proc-working.do")
	public ModelAndView selectWorkingAjax(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("jsonView");

		List<Map<String, Object>> list = visualService.selectWorkchild(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> list1 = visualService.selectWorkop(commandMap.getMap());
		mv.addObject("list1", list1);

		return mv;
	}

	@RequestMapping(value = "/visual/cctvfirst.do")
	public ModelAndView selectCctvfirst(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("workcctv/cctv");

		List<Map<String, Object>> list = visualService.selectCctvtraffic(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> list1 = visualService.selectCctvcp(commandMap.getMap());
		mv.addObject("list1", list1);

		// 8-3 교통CCTV 설치 현황
		List<Map<String, Object>> list3 = visualService.selectTrafficCctvState(commandMap.getMap());
		mv.addObject("list3", list3);

		// 8-4 방범CCTV 설치 현황
		List<Map<String, Object>> list4 = visualService.selectCrimeCctvState(commandMap.getMap());
		mv.addObject("list4", list4);

		return mv;

	}

	@RequestMapping(value = "/visual/proc-cctvfirst.do")
	public ModelAndView selectCCtvfirstAjax(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("jsonView");

		List<Map<String, Object>> list = visualService.selectCctvtraffic(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> list1 = visualService.selectCctvcp(commandMap.getMap());
		mv.addObject("list1", list1);

		return mv;
	}

	@RequestMapping(value = "/visual/fireplug.do")
	public ModelAndView selectFireplug(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("fireplug/illegal");

		List<Map<String, Object>> list = visualService.selectFireplug(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> year = visualService.selectFireplugYear(commandMap.getMap());
		mv.addObject("year", year);

		/* 4-1 행정동별 소화전 개수 */
		List<Map<String, Object>> list3 = visualService.selectFirePlugInfo(commandMap.getMap());
		mv.addObject("list3", list3);

		/* 4-3 행정동별 불법주차 단속 건수 */
		List<Map<String, Object>> list4 = visualService.selectIllegalParkingYY(commandMap.getMap());
		mv.addObject("list4", list4);

		/* 4-2 연도별월별 불법 주차 단속 현황 */
		List<Map<String, Object>> list5 = visualService.selectIllegalParkingState(commandMap.getMap());
		mv.addObject("list5", list5);

		return mv;

	}

	@RequestMapping(value = "/visual/proc-fireplug.do")
	public ModelAndView selectFireplugtAjax(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("jsonView");

		List<Map<String, Object>> list = visualService.selectFireplug(commandMap.getMap());
		mv.addObject("list", list);

		return mv;
	}

	@RequestMapping(value = "/visual/neglect.do")
	public ModelAndView selectNegltect(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("freight/neglect");

		List<Map<String, Object>> list = visualService.selectNeglect(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> year = visualService.selectNeglectYear(commandMap.getMap());
		mv.addObject("year", year);
		
		// 6-1 연도별월별무단방치차량단속현황
		List<Map<String, Object>> list33 = visualService.selectCarAccidentYY2(commandMap.getMap());
		mv.addObject("list33", list33);
		
		List<Map<String, Object>> list3_1 = visualService.selectCarAccidentYY("2018");
		List<Map<String, Object>> list3_2 = visualService.selectCarAccidentYY("2019");
		List<Map<String, Object>> list3_3 = visualService.selectCarAccidentYY("2020");
		mv.addObject("list3_1", list3_1);
		mv.addObject("list3_2", list3_2);
		mv.addObject("list3_3", list3_3);
		
		// 6-2행정동별무단방치차량현황
		List<Map<String, Object>> list4_1 = visualService.selectCarAccidentDo("이륜차");
		List<Map<String, Object>> list4_2 = visualService.selectCarAccidentDo("자동차");
		List<Map<String, Object>> list4_3 = visualService.selectCarAccidentDo("총합계");
		mv.addObject("list4_1", list4_1);
		mv.addObject("list4_2", list4_2);
		mv.addObject("list4_3", list4_3);

		return mv;

	}

	@RequestMapping(value = "/visual/proc-neglect.do")
	public ModelAndView selectNegltectAjax(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("jsonView");

		List<Map<String, Object>> list = visualService.selectNeglect(commandMap.getMap());
		mv.addObject("list", list);

		return mv;
	}

	@RequestMapping(value = "/visual/nightpark.do")
	public ModelAndView selectNightpark(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("freight/nightpark");

		List<Map<String, Object>> list = visualService.selectNightpark(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> year = visualService.selectNightparkYear(commandMap.getMap());
		mv.addObject("year", year);
		List<Map<String, Object>> list1 = visualService.selectNightpark1(commandMap.getMap());
		mv.addObject("list1", list1);
		List<Map<String, Object>> list2 = visualService.selectNightpark2(commandMap.getMap());
		mv.addObject("list2", list2);

		// 9-1월별화물차밤샘주차단속현황
		List<Map<String, Object>> list3_1 = visualService.selectIllegalParkingMM("2018");
		List<Map<String, Object>> list3_2 = visualService.selectIllegalParkingMM("2019");
		List<Map<String, Object>> list3_3 = visualService.selectIllegalParkingMM("2020");
		mv.addObject("list3_1", list3_1);
		mv.addObject("list3_2", list3_2);
		mv.addObject("list3_3", list3_3);

		// 9-2 행정동별 불법주차 단속건수
		List<Map<String, Object>> list4 = visualService.selectIllegalParkingDo(commandMap.getMap());
		mv.addObject("list4", list4);

		return mv;

	}

	@RequestMapping(value = "/visual/proc-nightpark.do")
	public ModelAndView selectNightparkAjax(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("jsonView");

		List<Map<String, Object>> list = visualService.selectNightpark(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> list1 = visualService.selectNightpark1(commandMap.getMap());
		mv.addObject("list1", list1);
		List<Map<String, Object>> list2 = visualService.selectNightpark2(commandMap.getMap());
		mv.addObject("list2", list2);

		return mv;
	}

	@RequestMapping(value = "/visual/transit.do")
	public ModelAndView selectTransit(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("freight/transit");

		List<Map<String, Object>> list = visualService.selectTransit(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> list1 = visualService.selectTransit1(commandMap.getMap());
		mv.addObject("list1", list1);
		List<Map<String, Object>> list2 = visualService.selectTransit2(commandMap.getMap());
		mv.addObject("list2", list2);
		List<Map<String, Object>> dong = visualService.selectTransitDong(commandMap.getMap());
		mv.addObject("dong", dong);

		// 10-1행정동별화물차운송사업자현황
		List<Map<String, Object>> list3 = visualService.selectCargoState(commandMap.getMap());
		mv.addObject("list3", list3);

		// 10-2화물차운송유형별비중
		List<Map<String, Object>> list4 = visualService.selectCargoType(commandMap.getMap());
		mv.addObject("list4", list4);

		return mv;

	}

	@RequestMapping(value = "/visual/proc-transit.do")
	public ModelAndView selectTransitAjax(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("jsonView");

		List<Map<String, Object>> list = visualService.selectTransit(commandMap.getMap());
		mv.addObject("list", list);
		List<Map<String, Object>> list1 = visualService.selectTransit1(commandMap.getMap());
		mv.addObject("list1", list1);
		List<Map<String, Object>> list2 = visualService.selectTransit2(commandMap.getMap());
		mv.addObject("list2", list2);
		return mv;
	}
}
