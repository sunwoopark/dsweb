package ds.visual.service;

import java.util.List;
import java.util.Map;

public interface VisualService {

	List<Map<String, Object>> selectAccident(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectAccident2(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectAccidentYear(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectDisabled(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectFine(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectParking(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectTraffic(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectTraffic2(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectTrafficYear(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectComParking(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectComParking2(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectComParkingYear(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectHangfine(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectWorkchild(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectWorkop(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectCctvtraffic(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectCctvcp(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectFireplug(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectFireplugYear(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectNeglect(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectNeglectYear(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectNightpark(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectNightpark1(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectNightpark2(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectNightparkYear(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectTransit(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectTransit1(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectTransit2(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectTransitDong(Map<String, Object> map) throws Exception;

	
	// 210225_신규내용 추가
	// 1. 교통민원
	List<Map<String, Object>> selectComplaintTraffic(String std_yy) throws Exception;

	// 2. 주차민원
	List<Map<String, Object>> selectComplaintParking(String std_yy) throws Exception;

	// 3-1 월별교통사고발생건수
	List<Map<String, Object>> selectAccidentTrafficMM(String std_yy) throws Exception;

	// 3-2 요일별교통사고발생건수
	List<Map<String, Object>> selectAccidentTrafficDay(String std_yy) throws Exception;

	// 4-1 행정동별 소화전 개수
	List<Map<String, Object>> selectFirePlugInfo(Map<String, Object> map) throws Exception;

	// 4-3 행정동별 불법주차 단속 건수
	List<Map<String, Object>> selectIllegalParkingYY(Map<String, Object> map) throws Exception;

	// 4-2 연도별월별 불법주차 단속 현황
	List<Map<String, Object>> selectIllegalParkingState(Map<String, Object> map) throws Exception;

	// 5-2 행정동별 일평균 유동인구수 및 대상건물수 현황
	List<Map<String, Object>> selectIllegalParkingAvg(Map<String, Object> map) throws Exception;

	// 7-1 행정동별불법주정차CCTV현황
	List<Map<String, Object>> selectCctvCntDo(Map<String, Object> map) throws Exception;

	// 7-2 월별장애인주차구역불법주차단속현황
	List<Map<String, Object>> selectDisabledParkingMM(String std_yy) throws Exception;
	List<Map<String, Object>> selectDisabledParkingMM2(Map<String, Object> map) throws Exception;
	
	// 7-3 장애인주차구역불법주차 행정동별 단속현황
	List<Map<String, Object>> selectDisabledParkingState(Map<String, Object> map) throws Exception;

	// 7-4 행정동별장애인주차장현황
	List<Map<String, Object>> selectDisabledParkingDo(Map<String, Object> map) throws Exception;

	// 8-1 행정동별 어린이 노인 시설 현황
	List<Map<String, Object>> selectWalkBuilDo(String buil_type) throws Exception;

	// 8-2육교 설치현황
	List<Map<String, Object>> selectOverpassState(Map<String, Object> map) throws Exception;

	// 8-3 교통CCTV 설치 현황
	List<Map<String, Object>> selectTrafficCctvState(Map<String, Object> map) throws Exception;

	// 8-4 방범CCTV 설치 현황
	List<Map<String, Object>> selectCrimeCctvState(Map<String, Object> map) throws Exception;

	// 9-1월별화물차밤샘주차단속현황
	List<Map<String, Object>> selectIllegalParkingMM(String std_yy) throws Exception;

	// 9-2 행정동별 불법주차 단속건수
	List<Map<String, Object>> selectIllegalParkingDo(Map<String, Object> map) throws Exception;

	// 10-1행정동별화물차운송사업자현황
	List<Map<String, Object>> selectCargoState(Map<String, Object> map) throws Exception;

	// 10-2화물차운송유형별비중
	List<Map<String, Object>> selectCargoType(Map<String, Object> map) throws Exception;

	// 6-1 연도별월별무단방치차량단속현황 
	List<Map<String, Object>> selectCarAccidentYY(String std_yy) throws Exception;
	List<Map<String, Object>> selectCarAccidentYY2(Map<String, Object> map) throws Exception;


	// 6-2행정동별무단방치차량현황
	List<Map<String, Object>> selectCarAccidentDo(String car_type) throws Exception;

}
