<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>대전광역시 서구 시각화 서비스</title>
<link rel="stylesheet" href="../../main/common/css/style.css">
</head>
<body>
	<div id="wrap">
		<header id="header">
			<div class="header-top" >
				<h1 class="logo">
					<img src="../main/images/h1-logo.png" alt="대전광역시 서구"> <span>시각화
						서비스</span></a>
				</h1>
			</div>
			<div class="header-bi">
				<div class="bi">
					<img src="../main/images/h-bi.png" alt="행복동행 대전서구">
				</div>
			</div>
		</header>
		<div id="container" class="main">
			<div class="main-section">
				<ul>
					<li>
						<div class="ml-item" onclick="location.href='/visual/traffic.do'" style="cursor:pointer;">
							<img src="../main/images/mlist-ico1.png" alt="교통 / 주차 관련 민원 분석 ">
							<p class="tit">교통 / 주차 관련 민원 분석</p>
							<p>
								대전광역시 서구 교통/주차관련<br> 민원 분석 현황을 보여드립니다.
							</p>
							<a>바로가기</a>
						</div>
					</li>
					<li>
						<div class="ml-item" onclick="location.href='/visual/nightpark.do'" style="cursor:pointer;">
							<img src="../main/images/mlist-ico2.png" alt="화물차 밤샘주차 단속현황">
							<p class="tit">화물차 밤샘주차 단속현황</p>
							<p>
								대전광역시 서구 교통/주차관련<br> 민원 분석 현황을 보여드립니다.
							</p>
							<a>바로가기</a>
						</div>
					</li>
					<li>
						<div class="ml-item" onclick="location.href='/visual/accident.do'" style="cursor:pointer;">
							<img src="../main/images/mlist-ico3.png" alt="화물차 밤샘주차 단속현황">
							<p class="tit">교통사고 원인 및 현황</p>
							<p>
								대전광역시 서구 교통/주차관련<br> 민원 분석 현황을 보여드립니다.
							</p>
							<a>바로가기</a>
						</div>
					</li>
					<li>
						<div class="ml-item" onclick="location.href='/visual/fireplug.do'" style="cursor:pointer;">
							<img src="../main/images/mlist-ico4.png" alt="소화전 주변 불법 주차단속현황">
							<p class="tit">
								소화전 주변<br>불법 주차단속현황
							</p>
							<p>
								대전광역시 서구 교통/주차관련<br> 민원 분석 현황을 보여드립니다.
							</p>
							<a>바로가기</a>
						</div>
					</li>
					<li>
						<div class="ml-item" onclick="location.href='/visual/fine.do'" style="cursor:pointer;">
							<img src="../main/images/mlist-ico5.png" alt="교통과태료 현황">
							<p class="tit">교통과태료 현황</p>
							<p>
								대전광역시 서구 교통/주차관련<br> 민원 분석 현황을 보여드립니다.
							</p>
							<a>바로가기</a>
						</div>
					</li>
					<li><!--  -->
						<div class="ml-item" onclick="location.href='/visual/neglect.do'" style="cursor:pointer;">
							<img src="../main/images/mlist-ico6.png" alt="무단방치차량 단속현황">
							<p class="tit">무단방치차량 단속현황</p>
							<p>
								대전광역시 서구 교통/주차관련<br> 민원 분석 현황을 보여드립니다.
							</p>
							<a>바로가기</a>
						</div>
					</li>
					<li>
						<div class="ml-item" onclick="location.href='/visual/transit.do'" style="cursor:pointer;">
							<img src="../main/images/mlist-ico7.png" alt="화물차 운송사업자 현황">
							<p class="tit">화물차 운송사업자 현황</p>
							<p>
								대전광역시 서구 교통/주차관련<br> 민원 분석 현황을 보여드립니다.
							</p>
							<a>바로가기</a>
						</div>
					</li>
					<li>
						<div class="ml-item" onclick="location.href='/visual/working.do'" style="cursor:pointer;">
							<img src="../main/images/mlist-ico8.png" alt="교통 보행환경 개선">
							<p class="tit">교통 보행환경 개선</p>
							<p>
								대전광역시 서구 교통/주차관련<br> 민원 분석 현황을 보여드립니다.
							</p>
							<a>바로가기</a>
						</div>
					</li>
					<li>
						<div class="ml-item" onclick="location.href='/visual/disabled.do'" style="cursor:pointer;">
							<img src="../main/images/mlist-ico9.png"
								alt="장애인 주차구역 불법 주정차 단속현황">
							<p class="tit">
								장애인 주차구역<br>불법 주정차 단속현황
							</p>
							<p>
								대전광역시 서구 교통/주차관련<br> 민원 분석 현황을 보여드립니다.
							</p>
							<a>바로가기</a>
						</div>
					</li>
					<li>
						<div class="ml-item" onclick="location.href='/visual/cctvfirst.do'" style="cursor:pointer;">
							<img src="../main/images/mlist-ico10.png" alt="교통및 방범 CCTV설치 우선지역">
							<p class="tit">
								교통및 방범<br>CCTV설치 우선지역
							</p>
							<p>
								대전광역시 서구 교통/주차관련<br> 민원 분석 현황을 보여드립니다.
							</p>
							<a>바로가기</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<footer id="footer">
			<div class="static">
				<address>대전광역시 서구 둔산서로 100(둔산동)</address>
				<span class="tel">대표전화: 042)288-2114 / 팩스: 042)288-5900</span> <span
					class="copy">Copyright(c) 2014 Seo-gu Office. All rights
					reserved.</span>
			</div>
		</footer>
	</div>
</body>
</html>