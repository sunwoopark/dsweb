<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/include-header-visual.jspf"%>
<div class="contents-top">
	<div>
		<a href="/visual/accident.do" class="btn-md on">교통사고 현황</a>
	</div>
</div>
<div class="h-group2">
	<div>
		<h3>사고내용별 교통사고 발생 건수</h3>
	</div>
	<div>
		<span class="tit">연도</span> <select id="std_yy" name="std_yy"
			onchange="chart1(); chart2();">
			<c:forEach var="year" items="${year}" varStatus="status">
				<option value="${year.std_yy }">${year.std_yy }</option>
			</c:forEach>
		</select>
	</div>
</div>
<div class="section">
	<div class="grid-full" id="line"></div>
	<div class="h-group2">
		<div>
			<h3>사고내용별 어린이 교통사고 발생 건수</h3>
		</div>
	</div>
	<div class="grid-full" id="bar"></div>
	
	<!-- 연도별 및 신청경로별 민원 현황 시작 -->
	<div class="h-group2">
		<div>
			<h3>월별 교통사고 발생 건수</h3>
		</div>
	</div>
	<div class="grid-full">
		<canvas id='Chart3' height='120px;'></canvas>
	</div>


	<div class="ect1">
		<table class="table" style="text-align: center;">
			<colgroup>
				<col style="width: 10%;">
				<col style="width: 30%;">
				<col style="width: 30%;">
				<col style="width: 30%;">
			</colgroup>
			<thead>
				<tr>
					<th>월</th>
					<th>2018년</th>
					<th>2019년</th>
					<th>총합계</th>
				</tr>
			</thead>

			<c:set var="sum_one" value="0" />
			<c:set var="sum_two" value="0" />
			<c:set var="sum_three" value="0" />

			<tbody>
				<c:forEach items="${list3_1 }" var="list3_1" varStatus="status">
					<tr>
						<td>${list3_1.std_mm }월</td>
						<td><fmt:formatNumber value="${list3_1.cnt}" pattern="#,###" /></td>
						<td><fmt:formatNumber value="${list3_2[status.index].cnt}" pattern="#,###" /></td>
						<td><fmt:formatNumber value="${list3_3[status.index].cnt}" pattern="#,###" /></td>
					</tr>
					<c:set var="sum_one" value="${sum_one + list3_1.cnt}" />
					<c:set var="sum_two" value="${sum_two + list3_2[status.index].cnt}" />
					<c:set var="sum_three" value="${sum_three + list3_3[status.index].cnt}" />
				</c:forEach>
				<tr>
					<td>합 계</td>
					<td><fmt:formatNumber value="${sum_one}" pattern="#,###" /></td>
					<td><fmt:formatNumber value="${sum_two}" pattern="#,###" /></td>
					<td><fmt:formatNumber value="${sum_three}" pattern="#,###" /></td>
				</tr>
			</tbody>
		</table>
	</div>
	<!-- 연도별 및 신청경로별 민원 현황 종료 -->
	
	<!-- 요일별교통사고발생건수 시작 -->
	<div class="h-group2">
		<div>
			<h3>요일별 교통사고 발생 건수</h3>
		</div>
	</div>
	<div class="grid-full">
		<canvas id='Chart4' height='120px;'></canvas>
	</div>


	<div class="ect1">
		<table class="table" style="text-align: center;">
			<colgroup>
				<col style="width: 10%;">
				<col style="width: 30%;">
				<col style="width: 30%;">
				<col style="width: 30%;">
			</colgroup>
			<thead>
				<tr>
					<th>요일</th>
					<th>2018년</th>
					<th>2019년</th>
					<th>총합계</th>
				</tr>
			</thead>

			<c:set var="sum_one" value="0" />
			<c:set var="sum_two" value="0" />
			<c:set var="sum_three" value="0" />

			<tbody>
				<c:forEach items="${list4_1 }" var="list4_1" varStatus="status">
					<tr>
						<td>${list4_1.day_nm }</td>
						<td><fmt:formatNumber value="${list4_1.cnt}" pattern="#,###" /></td>
						<td><fmt:formatNumber value="${list4_2[status.index].cnt}" pattern="#,###" /></td>
						<td><fmt:formatNumber value="${list4_3[status.index].cnt}" pattern="#,###" /></td>
					</tr>
					<c:set var="sum_one" value="${sum_one + list4_1.cnt}" />
					<c:set var="sum_two" value="${sum_two + list4_2[status.index].cnt}" />
					<c:set var="sum_three" value="${sum_three + list4_3[status.index].cnt}" />
				</c:forEach>
				<tr>
					<td>총합계</td>
					<td><fmt:formatNumber value="${sum_one}" pattern="#,###" /></td>
					<td><fmt:formatNumber value="${sum_two}" pattern="#,###" /></td>
					<td><fmt:formatNumber value="${sum_three}" pattern="#,###" /></td>
				</tr>
			</tbody>
		</table>
	</div>
	<!-- 요일별교통사고발생건수 종료 -->
	
	
	
	
</div>
<%@ include file="/WEB-INF/include/include-footer-visual.jspf"%>
<script src="../visual/common/js/Chart.min.js"></script>
<script>
	Chart.defaults.global.legend.labels.usePointStyle = true;
	chart1();
	chart2();


	function chart1() {
		var std_yy = $('#std_yy').val();
		var minor = new Array();
		var injury = new Array();
		var die = new Array();
		var serious = new Array();
		var label = new Array();
		$("#line").html("<canvas id='Chart1' height='120px;'></canvas>");
		$
				.ajax({
					url : '/visual/proc-accident.do',
					type : 'post',
					data : {
						std_yy : std_yy
					},
					success : function(data) {
						var ctx = "";
						$.each(data.list, function(key, values) {
							if (`\${values.acc_ty1}` == '중상사고') {
								serious.push(`\${values.cnt}`);
								label.push(`\${values.std_mm}`);
							} else if (`\${values.acc_ty1}` == '사망사고') {
								die.push(`\${values.cnt}`);
							} else if (`\${values.acc_ty1}` == '경상사고') {
								minor.push(`\${values.cnt}`);
							} else if (`\${values.acc_ty1}` == '부상신고사고') {
								injury.push(`\${values.cnt}`);
							}
						});

						var mydata = {
							labels : label,

							datasets : [ {
								label : '사망사고',
								data : die,
								borderColor : "#2914FF",
								fill : false,
								lineTension : 0,
								pointBackgroundColor : "#2914FF"
							}, {
								label : '중상사고',
								data : serious,
								borderColor : "#6322E6",
								fill : false,
								lineTension : 0,
								pointBackgroundColor : "#6322E6"
							}, {
								label : '부상신고사고',
								data : injury,
								borderColor : "#9D19FC",
								fill : false,
								lineTension : 0,
								pointBackgroundColor : "#9D19FC"
							}, {
								label : '경상사고',
								data : minor,
								borderColor : "#CA02F5",
								fill : false,
								lineTension : 0,
								pointBackgroundColor : "#CA02F5"
							}

							]
						};

						var op = {
							title : {
								display : false
							},
							legend : {
								position : 'bottom'
							},
							scales : {
								xAxes : [ {
									display : true
								} ],
								yAxes : [ {
									display : true,
									ticks : {
										userCallback : function(value, index,
												values) {
											value = value.toString().replace(
													regexp, ',');
											return value;
										},
									}
								} ]
							},
							annotation : {
								drawTime : 'afterDatasetsDraw',
								annotations : [ {
									type : 'line',
									mode : 'vertical',
									scaleID : 'y-axis-0',
									value : '78000 ',
									borderColor : 'green',
									borderWidth : 1,
									label : {
										enabled : true,
										position : "center"
									}
								} ]
							},
							tooltips : {
								mode : 'index',
								callbacks : {
									label : function(tooltipItem, data) {
										var label = data.datasets[tooltipItem.datasetIndex].label
												|| '';

										if (label) {
											label += ': ';
										}
										let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
										value = value.toString().replace(
												regexp, ',');
										return label + value;
									}
								}
							}
						};

						var ctx = document.getElementById("Chart1").getContext(
								"2d");
						var myChart = new Chart(ctx, {
							type : 'line',
							data : mydata,
							options : op
						});
					},
					error : function() {
						alert("aaa");
					}
				})

	}
	function chart2() {
		var std_yy = $('#std_yy').val(); // 연도
		var minor = new Array(); // 경상
		var injury = new Array(); //부상신고
		var die = new Array(); //사망
		var serious = new Array(); //중상
		var label = new Array();
		$("#bar").html("<canvas id='Chart2' height='80px;'></canvas>");
		$
				.ajax({
					url : '/visual/proc-accident.do',
					type : 'post',
					data : {
						std_yy : std_yy
					},
					success : function(data) {
						var ctx = "";
						$.each(data.list2, function(key, values) {
							if (`\${values.acc_ty1}` == '중상사고') {
								serious.push(`\${values.cnt}`);
								label.push(`\${values.std_mm}`);
							} else if (`\${values.acc_ty1}` == '사망사고') {
								die.push(`\${values.cnt}`);
							} else if (`\${values.acc_ty1}` == '경상사고') {
								minor.push(`\${values.cnt}`);
							} else if (`\${values.acc_ty1}` == '부상신고사고') {
								injury.push(`\${values.cnt}`);
							}
						});
						var mydata = {
							labels : label,

							datasets : [ {

								label : '사망사고',
								data : die,
								backgroundColor : "#2914FF",
								hoverBackgroundColor : "#2914FF"
							}, {
								label : '중상사고',
								data : serious,
								backgroundColor : "#6322E6",
								hoverBackgroundColor : "#6322E6"
							}, {
								label : '부상신고사고',
								data : injury,
								backgroundColor : "#9D19FC",
								hoverBackgroundColor : "#9D19FC"
							}, {

								label : '경상사고',
								data : minor,
								backgroundColor : "#CA02F5",
								hoverBackgroundColor : "#CA02F5"
							} ]
						};

						var op = {
							title : {
								display : false,
								text : '출생/사망 그래프'
							},
							legend : {
								position : 'bottom'
							},
							tooltips : {
								mode : 'index',
								callbacks : {
									label : function(tooltipItem, data) {
										var label = data.datasets[tooltipItem.datasetIndex].label
												|| '';

										if (label) {
											label += ': ';
										}
										let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
										value = value.toString().replace(
												regexp, ',');
										return label + value;
									}
								}
							},
							scales : {
								yAxes : [ {
									ticks : {
										beginAtZero : true,
										userCallback : function(value, index,
												values) {
											value = value.toString().replace(
													regexp, ',');
											return value;
										},
									}
								} ],
								xAxes : [ {} ],
							}
						};

						var ctx = document.getElementById("Chart2").getContext(
								"2d");
						var myChart = new Chart(ctx, {
							type : 'bar',
							data : mydata,
							options : op
						});
					},
					error : function() {
						alert("aaa");
					}
				})
	}
	
	function chart3() {
		
		var std_mm = new Array(); // 항목명칭(경로)
		var cnt_2018 = new Array(); // 민원수
		var cnt_2019 = new Array(); // 민원수

		<c:forEach items="${list3_1}" var="list3_1" varStatus="status">
			std_mm.push('${list3_1.std_mm}' + '월');
			cnt_2018.push(${list3_1.cnt});
			cnt_2019.push(${list3_2[status.index].cnt});
		</c:forEach>
	    
		var mydata = {
	        labels: std_mm , 
	        datasets: [{
	            label: '2018년',
	            data: cnt_2018,
	            backgroundColor: "navy",
	            hoverBackgroundColor: "navy"
	        } , {
	            label: '2019년',
	            data: cnt_2019,
	            backgroundColor: "Green",
	            hoverBackgroundColor: "Green"
	        } ]
	    };
	    var op = { 		
	        legend: {
	            position: 'top',
            	onClick: null	
	        },
	        tooltips: {
	        	enabled: false
	        },
	        hover: {
				animationDuration: 0
			},
			animation: { // 차트 위에 숫자
				duration: 1,
				onComplete: function () {
					var chartInstance = this.chart,
						ctx = chartInstance.ctx;
					ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
					ctx.fillStyle = 'black';
					ctx.textAlign = 'center';
					ctx.textBaseline = 'bottom';

					this.data.datasets.forEach(function (dataset, i) {
						var meta = chartInstance.controller.getDatasetMeta(i);
						meta.data.forEach(function (bar, index) {
							var data = dataset.data[index];							
							ctx.fillText(data, bar._model.x, bar._model.y - 5);
						});
					});
				}
			},
	        scales: {
	            yAxes: [{
	                 ticks: {
	                	 suggestedMax: 400, // 최대값
	                     beginAtZero: true,
	                     fontSize : 14, // 폰트 사이즈
	                     userCallback: function(value, index, values) {
	                         value = value.toString().replace(regexp, ',');
	                         return value;
	                     },
	                 }
	             }],
	             xAxes: [{}],
	        }
	    };

		// chart3 설정 최종
	    var ctx = document.getElementById("Chart3").getContext("2d");
	    var myChart = new Chart(ctx, {
	        type: 'bar',
	        data: mydata,
	        options: op
	    });
	}

	function chart4() {
		var day_nm = new Array(); // 항목명칭(경로)
		var cnt_2018 = new Array(); // 민원수
		var cnt_2019 = new Array(); // 민원수

		<c:forEach items="${list4_1}" var="list4_1" varStatus="status">
			day_nm.push('${list4_1.day_nm}');
			cnt_2018.push(${list4_1.cnt});
			cnt_2019.push(${list4_2[status.index].cnt});
		</c:forEach>
	    
		var mydata = {
	        labels: day_nm ,
	        datasets: [{
	            label: '2018',
	            data: cnt_2018,
	            backgroundColor: "navy",
	            hoverBackgroundColor: "navy"
	        } , {
	            label: '2019',
	            data: cnt_2019,
	            backgroundColor: "green",
	            hoverBackgroundColor: "green"
	        }]
	    };
	    var op = { 		
	        legend: {
	            position: 'top',
	            onClick: null	
	        },
	        tooltips: {
	        	enabled: false
	            /* mode: 'index',
	            callbacks: {
	                label: function(tooltipItem, data) {
	                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

	                    if (label) {
	                        label += ': ';
	                    }
	                    let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                    value = value.toString().replace(regexp, ',');
	                    return label + value;
	                }
	            } */
	        },
	        hover: {
				animationDuration: 0
			},
			animation: { // 차트 위에 숫자
				duration: 1,
				onComplete: function () {
					var chartInstance = this.chart,
						ctx = chartInstance.ctx;
					ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
					ctx.fillStyle = 'black';
					ctx.textAlign = 'center';
					ctx.textBaseline = 'bottom';

					this.data.datasets.forEach(function (dataset, i) {
						var meta = chartInstance.controller.getDatasetMeta(i);
						meta.data.forEach(function (bar, index) {
							var data = dataset.data[index];							
							ctx.fillText(data, bar._model.x, bar._model.y - 5);
						});
					});
				}
			},
	        scales: {
	            yAxes: [{
	                 ticks: {
	                	 suggestedMax: 500, // 최대값
	                     beginAtZero: true,
	                     fontSize : 14, // 폰트 사이즈
	                     userCallback: function(value, index, values) {
	                         value = value.toString().replace(regexp, ',');
	                         return value;
	                     },
	                 }
	             }],
	             xAxes: [{}],
	        }
	    };

		// chart4 설정 최종
	    var ctx = document.getElementById("Chart4").getContext("2d");
	    var myChart = new Chart(ctx, {
	        type: 'bar',
	        data: mydata,
	        options: op
	    });
	}

	chart3();
	chart4();
	
</script>