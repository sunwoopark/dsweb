<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<title>대전시 관리자</title>

		<!-- UI Definition -->
		<!--  CSS -->
		<link rel="stylesheet" href="/common/css/jquery-ui.css">
	    <link rel="stylesheet" href="/common/css/style.css">

		<!-- jQuery -->
		<script src="/common/js/jquery-3.2.1.min.js"></script>
	    <script src="/common/js/jquery-ui.js"></script>
	    <script src="/common/js/common.js"></script>

    	<!-- Biz Process -->
	    <script src="/js/custom.js"></script>
	</head>
	<body>
		<p id="accessibility"><a href="#container">본문바로가기</a></p>
		<div id="wrap">
		    <header id="header">
		        <div class="static">
		            <h1><a href="/login.do">대전시</a></h1>
		            <div>
		               <h2>로그인</h2>
		            </div>
		        </div>      
		    </header>
		    
		    <div id="container">
		       <div class="contents">
					<div class="h-group">
						<h3 class="h-ty1 hty1">회원가입</h3>
						<p class="noti"><em>*</em> 표시는 필수입력 항목입니다.</p>
					</div>
					<form id="frm">
						<div class="list-tb row mbr-tb">
							<table>
								<colgroup>
									<col style="width:15%">
									<col style="width:35%">
									<col style="width:15%">
									<col style="width:35%">
								</colgroup>
								<tbody>
									<tr>
										<th><em>*</em>ID</th>
										<td colspan="3">
											<input type="text" class="ipt-email" maxlength="20" id="user_id" name="user_id">
											<button type="button" class="btn-md black" onclick="fn_emailCheck() ; return false ;">중복확인</button>
											<input type="hidden" id="emailChk" value="N">
										</td>
									</tr>
									<tr>
										<th><em>*</em>비밀번호</th>
											<td>
												<input type="password" placeholder="영문, 숫자 포함 8자 이상" class="full" maxlength="20" id="user_passwd" name="user_passwd">
											</td>
										<th><em>*</em>비밀번호 확인</th>
										<td>
											<div class="pass-re">
												<input type="password" class="full" maxlength="20" id="user_passwd2">
											</div>
										</td>
									</tr>
									<tr>
										<th><em>*</em>부서</th>
										<td>
											<input type="text" class="full" maxlength="20" id="user_dpnm" name="user_dpnm">
										</td>
										<th><em>*</em>연락처</th>
										<td>
											<div class="tel-wp">
												<select id = "tono" name="tono">
													<option value="">지역번호</option>
													<c:forEach var="tonolList" items="${tonolList}" varStatus="status">
														<option value="${tonolList.minor_cd }">${tonolList.minor_nm }</option>
													</c:forEach>
												</select>
												<input type="text" maxlength="4" id="user_tlno1" name="user_tlno1" onkeyup="fn_onlyNumber(this)" >
												<input type="text" maxlength="4" id="user_tlno2" name="user_tlno2" onkeyup="fn_onlyNumber(this)" >
											</div>
										</td>
									</tr>
									<tr>
										<th><em>*</em>직급</th>
										<td>
											<input type="text" class="full" maxlength="20" id="user_olnm" name="user_olnm">
										</td>
										<th><em>*</em>성함</th>
										<td>
											<input type="text" class="full" maxlength="20" id="user_nm" name="user_nm">
										</td>
									</tr>
								</tbody>
							</table>
						</div>	<!-- .list-tb -->
					</form>
					<div class="btns-btm-center">
						<button type="submit" class="btn-lg blue" onclick="fn_reqApprval() ; return false ;">승인요청</button>
					</div>
					<div class="join-btm">
						<img src="../images/bg-mbr.png" alt="">
					</div>
							
					<!-- 가입완료. 화면 이동때문에 공통 처리 안됨. -->
					<div class="layer-pop" id="joinComplete">
						<div class="alert-pop">
							<div class="alert-pop-head">
								<button type="button" class="alert-close">닫기</button>
							</div>
							<div class="alert-pop-body">
								<div class="msg">
									<strong class="tx2">관리자에게 승인 요청을 보냈습니다.</strong>
								</div>
								<div class="btm">
									<div class="btm-btns">
										<button type="button" class="btn-md black" id="joinConfirm">확인</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--  -->
				</div>
				<!-- content -->
			</div>
			<!-- container -->	
		</div>
		<!-- wrap -->
	
		<!-- Layer Popup Div Import -->
		<%@ include file="/WEB-INF/include/include-layerpopup.jspf" %>
		
		<script type="text/javascript">
			"use strict" ;
				
			$(document).ready(function(){
					
				// 관리자승인 요청 완료 확인 button click
				$("#joinConfirm").on("click",function() {
					$('#joinComplete').hide();
					location.href = "/login.do" ;
				});
			
			});
		
			// Email 중복 Check
			function fn_emailCheck() {
					
				if (!$("#user_id").val() ) {
					fn_openType2LayerPop("user_id") ;
					return ;
				}
					
				$.ajax({
					url : "/login/proc-check-email.do",
					type : "POST",
					data : {
						user_id : $("#user_id").val(),
					},
					dataType : "json",
					success : function(data){
						if (data.result == "0"){
							fn_openType1LayerPop("acceptEmail") ;
						} else {
							fn_openType2LayerPop("dupedEmail") ;
						}
					},
					error : function(data) {}
				});
					
			}
				
			function fn_reqApprval() {
					
				// 필수입력 Check
				if (!$("#user_id").val()) {
					// 이메일 입력 여부
					fn_openType2LayerPop("user_id") ;
					return ;
				} else if (!$("#user_passwd").val()) {
					// 비밀번호1 입력 여부
					fn_openType2LayerPop("user_passwd") ;
					return ;
				} else if (!$("#user_passwd2").val()) {
					// 비밀번호2 입력 여부
					fn_openType2LayerPop("user_passwd2") ;
					return ;
				} else if (!$("#user_dpnm").val()) {
					// 부서 입력 여부
					fn_openType2LayerPop("user_dpnm") ;
					return ;
				} else if (!$("#tono").val()) {
					// 지역번호 선택 여부
					fn_openType2LayerPop("tono") ;
					return ;
				} else if (!$("#user_tlno1").val()) {
					// 연락처1 입력 여부
					fn_openType2LayerPop("user_tlno1") ;
					return ;
				} else if (!$("#user_tlno2").val()) {
					// 연락처2 입력 여부
					fn_openType2LayerPop("user_tlno2") ;
					return ;
				} else if (!$("#user_olnm").val()) {
					// 직급 입력 여부
					fn_openType2LayerPop("user_olnm") ;
					return ;
				} else if (!$("#user_nm").val()) {
					// 성함 입력 여부
					fn_openType2LayerPop("user_nm") ;
					return ;
				}
					
				// 비밀번호 정합성 Check
				// 비밀번호 정합성 Check용 변수
				const pattern1 = /[0-9]/;	// 숫자
				const pattern2 = /[a-z]/;	// 소문자
				const vPassword = $("#user_passwd").val();
				
				if($("#emailChk").val() == "N") {
					// 이메일중복 Check 여부 확인
					fn_openType3LayerPop("chkEmailDup") ;
					return ;
				} else if ($("#user_passwd").val() != $("#user_passwd2").val()) {
					// 비밀번호 동일성 Check
					fn_openType2LayerPop("chkSamePasswd") ;
					return ;
				} else if (vPassword.length < 8 ) {
					// 길이 Check
					fn_openType2LayerPop("passwdLengthChk") ;
					return ;
				} else if (!pattern1.test(vPassword) && !pattern2.test(vPassword)){
					// 소문자와 숫자 포함
					fn_openType2LayerPop("passwdCombChk") ;
					return ;
				} 
			
				// 가입처리
				const comAjax = new ComAjax("frm");
					
				comAjax.setUrl("<c:url value='/login/proc-regist-user.do' />");
				comAjax.setCallback("fn_reqApprvalCallback");
					
				comAjax.ajax();
					
			}
				
			function fn_reqApprvalCallback(data) {
					
				const vResultCode = data.resultCode ;
				const vResultMessage = data.resultMsg ;
					
				if (vResultCode != "ok") {
					fn_openType1LayerPop("joinFail") ;
					return ;
				}
					
				fn_openLayerPop("joinComplete") ;
					
			}
				
			function fn_openLayerPop(pType) {
				switch (pType) {
				case "joinComplete":
					$('#joinComplete').show();
					break ;
				default:		
				}
				
			}
				
			// Type1 Layer Popup 공통 처리
			// Message가 한 줄이고 큰 글자인 경우
			function fn_openType1LayerPop(pType) {
				let msg1 ;
					
				switch (pType) {
				case "acceptEmail":
					$("#emailChk").val("Y") ;
					msg1 = "가입 가능한 ID 입니다." ;
					break;
				case "joinFail":
					msg1 = "회원가입 처리 시 오류가 발생하였습니다." ;
					break;
				default:
				}
					
				$("#type1Msg").html(msg1) ;
				$("#layerPopType1").show() ;
					
			}
				
			// Type2 Layer Popup 공통 처리
			// Message가 두 줄인 경우
			function fn_openType2LayerPop(pType) {
					
				let msg1 ;
				let msg2 ;
					
				switch (pType) {
				case "user_id":
					msg1 = "ID를 입력하세요." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				case "user_passwd":
					msg1 = "앞 비밀번호를 입력하세요." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break ;
				case "user_passwd2":
					msg1 = "뒤 비밀번호를 입력하세요." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				case "user_dpnm":
					msg1 = "부서를 입력하세요." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				case "tono":
					msg1 = "지역번호를 선택해주세요." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				case "user_tlno1":
					msg1 = "국번을 입력하세요." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				case "user_tlno2":
					msg1 = "전화번호를 입력하세요." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				case "user_olnm":
					msg1 = "직급을 입력하세요." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				case "user_nm":
					msg1 = "성함을 입력하세요." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				case "dupedEmail":
					$("#emailChk").val("N") ;
					msg1 = "이미 가입한 ID 입니다." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				case "chkSamePasswd":
					msg1 = "비밀번호가 다릅니다." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				case "passwdLengthChk":
					msg1 = "비밀번호의 길이는 8자리 이상이어야 합니다." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				case "passwdCombChk":
					msg1 = "비밀번호는 소문자와 숫자를 포함해야 합니다." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				default:
						
				}
					
				$("#type2Msg1").html(msg1) ;
				$("#type2Msg2").html(msg2) ;
					
				$("#layerPopType2").show() ;
					
			}
				
			// Type3 Layer Popup 공통 처리
			// Message가 한 줄이고 보통 글자인 경우
			function fn_openType3LayerPop(pType) {
				let msg1 ;
					
				switch (pType) {
				case "chkEmailDup":
					msg1 = "ID중복여부를 확인하시기 바랍니다." ;
					break;
				default:
				}
					
				$("#type3Msg").html(msg1) ;
				$("#layerPopType3").show() ;
					
			}
				
			// 숫자만 입력
			let prev = ""; 
			let regexp2 = /^\d*(\.\d{0,2})?$/;
			function fn_onlyNumber(obj) {
				if(obj.value.search(regexp2)==-1) {
					obj.value = prev;
				} else {
					prev = obj.value;
				}
			}
		
		</script>
	</body>
</html>