<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <title>대전시 관리자</title>
	    
	    <!-- UI Definition -->
		<!--  CSS -->
		<link rel="stylesheet" href="/common/css/jquery-ui.css">
	    <link rel="stylesheet" href="/common/css/style.css">
		
		<!-- jQuery -->
		<script src="/common/js/jquery-3.2.1.min.js"></script>
	    <script src="/common/js/jquery-ui.js"></script>
	    <script src="/common/js/common.js"></script>
	    
	    <script src="/js/custom.js"></script>
	    
	</head>
	<body>
		<p id="accessibility"><a href="#container">본문바로가기</a></p>   
		<div id="wrap">
			<header id="header">
				<div class="static">
					<h1><a href="javaScript:void(0);">대전시</a></h1>
					<div>
						<h2>관리자 로그인</h2>
					</div>
				</div>
			</header>
			<div id="container">
				<div class="contents login-content">
					<div class="sadm-gate">
						<!-- 업무 메인 나오면 Link 걸 것!! -->
						<a href="/lvst/main.do" class="gate1">축산</a>
						<a href="/mdcl/main.do" class="gate2">의료</a>
						<a href="/farm/list.do" class="gate3">농업</a>
					</div>
				</div>
				<!-- //contents -->
			</div>
			<!-- //container -->
			
			<footer id="footer"><address>(우)35238 대전광역시 서구 둔산서로 100(둔산동)</address><span class="tel">대표전화 : 042)288-2114/ 팩스 : 042)288-5900</span>
            <span class="copy">Copyright (C) 2014 Seo-gu Office. All rights reserved.</span>
			</footer>
		</div>
	</body>
</html>