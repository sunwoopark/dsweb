<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/include-header-visual.jspf"%>

<div class="contents-top">
	<div class="lnbmenu">
		<a href="/visual/working.do" class="btn-md off">교통보행환경 개선</a> <a
			href="/visual/cctvfirst.do" class="btn-md on">CCTV 설치우선 지역</a>
	</div>
</div>
<div class="h-group2">
	<div>
		<h3>교통보행환경</h3>
	</div>
	<div class="btn-nbd">
		<button class="btn-nb2 on" onclick="chart1(); chart3();">교통CCTV</button>
		<button class="btn-nb2 off" onclick="chart2(); chart4();">방범CCTV</button>
	</div>
</div>
<div class="section">
	<div class="grid-full" id="mapwrap"></div>

	<div id="testMap"></div>


</div>


<%@ include file="/WEB-INF/include/include-footer-visual.jspf"%>
<script src="../visual/common/js/Chart.min.js"></script>
<script type="text/javascript"
	src="//dapi.kakao.com/v2/maps/sdk.js?appkey=cc46f2d48285e645648b9d4a912d6346"></script>
<script>
Chart.defaults.global.legend.labels.usePointStyle = true;
chart1();
$(function(){
    $('.btn-nbd>button').on('click',function(){
        $(this).parent().find('button').removeClass('on')
        $(this).parent().find('button').addClass('off')
        $(this).removeClass('off')
        $(this).addClass('on')
    });
});
function chart1(){
$("#mapwrap").html('<div style="width:1050px;height:520px;"><div id="map" style="width:1050px;height:500px;"></div></div>');
var col1 = $('#col1').val();
var regexp = /\B(?=(\d{3})+(?!\d))/g;
$.ajax({
	url:'/visual/proc-cctvfirst.do',
	type:'post',
	data:{},
	success:function(data){

var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
    mapOption = { 
	center: new kakao.maps.LatLng(36.3556250, 127.3837457), // 지도의 중심좌표
    level: 5 // 지도의 확대 레벨
    };
var imageSrc = '../visual/images/red_pin.png', // 마커이미지의 주소입니다    
imageSize = new kakao.maps.Size(8, 8), // 마커이미지의 크기입니다
imageOption = {offset: new kakao.maps.Point(8, 8)}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.
var markerArray = new Array();
//var markerImage = new kakao.maps.MarkerImage('../../../images/visual/blue_pin.png', imageSize, imageOption) // 마커가 표시될 위치입니다
//마커의 이미지정보를 가지고 있는 마커이미지를 생성합니다

var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption),
markerPosition = new kakao.maps.LatLng(37.54699, 127.09598); // 마커가 표시될 위치입니다

var map = new kakao.maps.Map(mapContainer, mapOption),
    infowindow = new kakao.maps.InfoWindow({removable: true});
var cont = new Array();
var llng = new Array();
$.each(data.list, function(key, values){
	cont.push('<div>주소 :'+ `\${values.addr}`+'</div>');
	llng.push(new kakao.maps.LatLng(`\${values.lo}` , `\${values.la}`));
})

var positions = [];
	for(var i = 0; i < llng.length; i ++){
		positions.push(
	    {
	        content: cont[i], 
	        latlng: llng[i]
	    },
	    )
	}


// 마커 이미지의 이미지 주소입니다
for (var i = 0; i < positions.length; i ++) {
//마커 이미지의 이미지 크기 입니다

    var marker = new kakao.maps.Marker({
        map: map, // 마커를 표시할 지도
        position: positions[i].latlng, // 마커를 표시할 위치
        image: markerImage
    });
    
    var infowindow = new kakao.maps.InfoWindow({
        content: positions[i].content // 인포윈도우에 표시할 내용
    });
    
    kakao.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
    kakao.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));
 
}

function makeOverListener(map, marker, infowindow) {
    return function() {
        infowindow.open(map, marker);
    };
}

// 인포윈도우를 닫는 클로저를 만드는 함수입니다 
function makeOutListener(infowindow) {
    return function() {
        infowindow.close();
    };
}

	}, error: function(){
		alert("aaa");
	}
})
}

function chart2(){
	$("#mapwrap").html('<div style="width:1050px;height:520px;"><div id="map" style="width:1050px;height:500px;"></div></div>');
	var col1 = $('#col1').val();
	var regexp = /\B(?=(\d{3})+(?!\d))/g;
	$.ajax({
		url:'/visual/proc-cctvfirst.do',
		type:'post',
		data:{},
		success:function(data){

	var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
	    mapOption = { 
		center: new kakao.maps.LatLng(36.3556250, 127.3837457), // 지도의 중심좌표
	    level: 5 // 지도의 확대 레벨
	    };
	var imageSrc = '../visual/images/red_pin.png', // 마커이미지의 주소입니다    
	imageSize = new kakao.maps.Size(8, 8), // 마커이미지의 크기입니다
	imageOption = {offset: new kakao.maps.Point(8, 8)}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.
	var markerArray = new Array();
	//var markerImage = new kakao.maps.MarkerImage('../../../images/visual/blue_pin.png', imageSize, imageOption) // 마커가 표시될 위치입니다
	//마커의 이미지정보를 가지고 있는 마커이미지를 생성합니다

	var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption),
	markerPosition = new kakao.maps.LatLng(37.54699, 127.09598); // 마커가 표시될 위치입니다

	var map = new kakao.maps.Map(mapContainer, mapOption),
	    infowindow = new kakao.maps.InfoWindow({removable: true});
	var cont = new Array();
	var llng = new Array();
	$.each(data.list1, function(key, values){
		cont.push('<div>주소 :'+ `\${values.addr}`+'</div>');
		llng.push(new kakao.maps.LatLng(`\${values.lo}` , `\${values.la}`));
	})

	var positions = [];
		for(var i = 0; i < llng.length; i ++){
			positions.push(
		    {
		        content: cont[i], 
		        latlng: llng[i]
		    },
		    )
		}


	// 마커 이미지의 이미지 주소입니다
	for (var i = 0; i < positions.length; i ++) {
	//마커 이미지의 이미지 크기 입니다

	    var marker = new kakao.maps.Marker({
	        map: map, // 마커를 표시할 지도
	        position: positions[i].latlng, // 마커를 표시할 위치
	        image: markerImage
	    });
	    
	    var infowindow = new kakao.maps.InfoWindow({
	        content: positions[i].content // 인포윈도우에 표시할 내용
	    });
	    
	    kakao.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
	    kakao.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));
	 
	}

	function makeOverListener(map, marker, infowindow) {
	    return function() {
	        infowindow.open(map, marker);
	    };
	}

	// 인포윈도우를 닫는 클로저를 만드는 함수입니다 
	function makeOutListener(infowindow) {
	    return function() {
	        infowindow.close();
	    };
	}

		}, error: function(){
			alert("aaa");
		}
	})
	}

</script>


<script>
function chart3() {
	$("#testMap").html('<div class="h-group2"><div><h3>교통CCTV 설치 현황</h3></div></div><div class="grid-full"><canvas id="Chart3" height="120px;"></canvas></div><div class="ect1"><table class="table" style="text-align: center;"><colgroup><col style="width: 40%;"><col style="width: 30%;"><col style="width: 30%;"></colgroup><thead><tr><th>행정동</th><th>불법주정차 단속CCTV</th><th>차량번호인식 CCTV</th></tr></thead><tbody><c:set var="sum_one" value="0" /><c:set var="sum_two" value="0" /><c:forEach items="${list3 }" var="list3"><tr><td>${list3.dong_nm }</td><td>${list3.illegal_cnt}</td><td>${list3.car_cnt}</td></tr><c:set var="sum_one" value="${sum_one + list3.illegal_cnt}" /><c:set var="sum_two" value="${sum_two + list3.car_cnt}" /></c:forEach><tr><td>총합계</td><td><fmt:formatNumber value="${sum_one}" pattern="#,###" /></td><td><fmt:formatNumber value="${sum_two}" pattern="#,###" /></td></tr></tbody></table></div>');
	
	var dong_nm = new Array();
	var illegal_cnt = new Array();
	var car_cnt = new Array();

	<c:forEach items="${list3}" var="list3">
		dong_nm.push('${list3.dong_nm}');
		illegal_cnt.push(${list3.illegal_cnt});
		car_cnt.push(${list3.car_cnt});
	</c:forEach>

    var mydata = {
        labels: dong_nm ,
        datasets: [{
            label: '불법주정차 단속CCTV',
            data: illegal_cnt ,
            backgroundColor: "navy",
            hoverBackgroundColor: "navy"
        }, {
            label: '차량번호인식CCTV',
            data: car_cnt ,
            backgroundColor: "green",
            hoverBackgroundColor: "green"
        }
        
        ]
    };
    var op = { 		
        legend: {
            position: 'top',
           	onClick: null	
        },
        tooltips: {
        	enabled: false
            /* mode: 'index',
            callbacks: {
                label: function(tooltipItem, data) {
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    value = value.toString().replace(regexp, ',');
                    return label + value;
                }
            } */ 
        },
        hover: {
			animationDuration: 0
		},
		animation: { // 차트 위에 숫자
			duration: 1,
			onComplete: function () {
				var chartInstance = this.chart,
					ctx = chartInstance.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = 'black';
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';

				this.data.datasets.forEach(function (dataset, i) {
					var meta = chartInstance.controller.getDatasetMeta(i);
					meta.data.forEach(function (bar, index) {
						var data = dataset.data[index];							
						ctx.fillText(data, bar._model.x, bar._model.y - 5);
					});
				});
			}
		},
        scales: {
            yAxes: [{
                 ticks: {
                	 suggestedMax: 15, // 최대값
                     beginAtZero: true,
                     fontSize : 14, // 폰트 사이즈
                     userCallback: function(value, index, values) {
                         value = value.toString().replace(regexp, ',');
                         return value;
                     },
                 }
             }],
             xAxes: [{}],
        }
    };

	// chart3 설정 최종
    var ctx = document.getElementById("Chart3").getContext("2d");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: mydata,
        options: op
    });
}

chart3();
</script>

<script>
function chart4() {
	$("#testMap").html('<div class="h-group2"><div><h3>방범 CCTV 설치 현황</h3></div></div><div class="grid-full"><canvas id="Chart4" height="120px;"></canvas></div><div class="ect1"><table class="table" style="text-align: center;"><colgroup><col style="width: 25%;"><col style="width: 25%;"><col style="width: 25%;"><col style="width: 25%;"></colgroup><thead><tr><th>행정동</th><th>방범 CCTV</th><th>행정동</th><th>방범 CCTV</th></tr></thead><tbody><c:set var="i" value="0" /><c:set var="j" value="2" /><c:forEach items="${list4 }" var="list4"><c:if test="${i%j == 0 }"><tr></c:if><td>${list4.dong_nm }</td><td>${list4.cnt}</td><c:if test="${i%j == j-1 }"></tr></c:if><c:set var="i" value="${i+1 }" /></c:forEach><td>-</td><td>-</td></tbody></table></div>');
	
	var dong_nm = new Array();
	var cnt = new Array();

	<c:forEach items="${list4}" var="list4">
		dong_nm.push('${list4.dong_nm}');
		cnt.push(${list4.cnt});
	</c:forEach>

    var mydata = {
        labels: dong_nm ,
        datasets: [{
            label: '불법주정차 단속CCTV',
            data: cnt ,
            backgroundColor: "navy",
            hoverBackgroundColor: "navy"
        } ]
    };
    var op = { 		
        legend: {
            position: 'top',
           	onClick: null	
        },
        tooltips: {
        	enabled: false
            /* mode: 'index',
            callbacks: {
                label: function(tooltipItem, data) {
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    value = value.toString().replace(regexp, ',');
                    return label + value;
                }
            } */ 
        },
        hover: {
			animationDuration: 0
		},
		animation: { // 차트 위에 숫자
			duration: 1,
			onComplete: function () {
				var chartInstance = this.chart,
					ctx = chartInstance.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = 'black';
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';

				this.data.datasets.forEach(function (dataset, i) {
					var meta = chartInstance.controller.getDatasetMeta(i);
					meta.data.forEach(function (bar, index) {
						var data = dataset.data[index];							
						ctx.fillText(data, bar._model.x, bar._model.y - 5);
					});
				});
			}
		},
        scales: {
            yAxes: [{
                 ticks: {
                	 suggestedMax: 100, // 최대값
                     beginAtZero: true,
                     fontSize : 14, // 폰트 사이즈
                     userCallback: function(value, index, values) {
                         value = value.toString().replace(regexp, ',');
                         return value;
                     },
                 }
             }],
             xAxes: [{}],
        }
    };

	// chart3 설정 최종
    var ctx = document.getElementById("Chart4").getContext("2d");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: mydata,
        options: op
    });
}


</script>
